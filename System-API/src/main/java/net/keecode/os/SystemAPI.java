package net.keecode.os;


import com.sun.management.OperatingSystemMXBean;

import java.lang.management.ManagementFactory;

/**
 * The API Class for interacting directly with the OS
 */
public class SystemAPI {

	private static SystemAPI systemAPI;

	private OperatingSystemMXBean osBean;

	private SystemAPI() {
		osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
	}

	/**
	 * Get the singleton instance of the SystemAPI
	 *
	 * @return The singleton instance
	 */
	public static SystemAPI getSystemAPI() {
		if (systemAPI == null)
			systemAPI = new SystemAPI();
		return systemAPI;
	}

	/**
	 * Returns the current System CPU load in percent
	 *
	 * @return The CPU load in percent
	 */
	public double getCPUUsage() {
		return osBean.getSystemCpuLoad() * 100;
	}

	/**
	 * Returns the total amount of RAM built-in this system
	 *
	 * @return The total amount of RAM
	 */
	public long getTotalRAM() {
		return osBean.getTotalPhysicalMemorySize();
	}

	/**
	 * Returns the amount of used RAM
	 *
	 * @return The used amount of RAM
	 */
	public long getUsedRAM() {
		return osBean.getTotalPhysicalMemorySize() - osBean.getFreePhysicalMemorySize();
	}

	/**
	 * Returns the amount of unused RAM
	 *
	 * @return The unused amount of RAM
	 */
	public long getFreeRAM() {
		return osBean.getFreePhysicalMemorySize();
	}

	/**
	 * Returns the amount of available processors
	 *
	 * @return The amount of available processors
	 */
	public int getProcessorAmount() {
		return osBean.getAvailableProcessors();
	}

	/**
	 * Returns the percentage of CPU usage this process is causing
	 *
	 * @return The percentage of CPU this process is causing
	 */
	public double getProcessCPUUsage() {
		return osBean.getProcessCpuLoad() * 100;
	}


	public long getProcessCPUTime() {
		return osBean.getProcessCpuTime();
	}

	/**
	 * Returns the amount of SWAP available
	 *
	 * @return The amount of availabe SWAP
	 */
	public long getFreeSwap() {
		return osBean.getFreeSwapSpaceSize();
	}

	/**
	 * Returns the architecture of the current system
	 *
	 * @return The current system architecture
	 */
	public String getArch() {
		return osBean.getArch();
	}


}
