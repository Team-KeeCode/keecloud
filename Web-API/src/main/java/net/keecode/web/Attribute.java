package net.keecode.web;

import org.w3c.dom.Attr;

/**
 * Created by Michael on 04.04.2016.
 */
public class Attribute {

	private String key;
	private String value;

	public Attribute(String key, String value){
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" ");
		sb.append(key);
		sb.append('=');
		sb.append('"');
		sb.append(value);
		sb.append('"');
		return sb.toString();
	}
}
