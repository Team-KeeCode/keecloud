package net.keecode.web;

import net.keecode.util.MashMap;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

/**
 * Created by Michael on 03.04.2016.
 */
public class WebAPI {

	private static WebAPI webAPI;

	private Server server;
	private MashMap<String, ServletContextHandler> endpoints = new MashMap<String, ServletContextHandler>();

	private WebAPI(){server = new Server(80);}

	public static WebAPI getWebAPI(){
		if(webAPI == null)
			webAPI = new WebAPI();
		return webAPI;
	}

	public ServletContextHandler addEndpoint(String endpoint){
		ServletContextHandler contextHandler = new ServletContextHandler(server, endpoint);
		endpoints.put(endpoint, contextHandler);
		return contextHandler;
	}

	public ServletContextHandler getEndpoint(String endpoint){
		return endpoints.getValue(endpoint);
	}

	public void start() {
		try{
			server.start();
		}catch (Exception ex){

		}
	}


}
