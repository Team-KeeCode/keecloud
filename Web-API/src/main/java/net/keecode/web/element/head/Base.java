package net.keecode.web.element.head;

import net.keecode.web.Attribute;
import net.keecode.web.element.HeadElement;

/**
 * Created by Michael on 04.04.2016.
 */
public class Base extends HeadElement {

	private Attribute href;
	private Attribute target;

	public Base(){
		super("base");
		href = new Attribute("href", "");
		target = new Attribute("target", "_blank");

		addAttribute(href);
		addAttribute(target);
	}

	public Attribute getHref() {
		return href;
	}

	public void setHref(Attribute href) {
		this.href = href;
	}

	public Attribute getTarget() {
		return target;
	}

	public void setTarget(Attribute target) {
		this.target = target;
	}

	@Override
	public String toString() {
		return getOpenTag();
	}
}
