package net.keecode.web.element.head;

import net.keecode.web.Attribute;
import net.keecode.web.element.HeadElement;
import net.keecode.web.element.HtmlElement;

import java.util.ArrayList;

/**
 * Created by Michael on 04.04.2016.
 */
public class Title extends HeadElement {

	private String title;

	public Title() {
		super("title");
	}

	@Override
	public void setContent(ArrayList<HtmlElement> content) {}

	@Override
	public void removeElement(int position) {}

	@Override
	public void removeElement(HtmlElement element) {}

	@Override
	public void addElement(HtmlElement element) {}

	@Override
	public ArrayList<HtmlElement> getContent() {return null;}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getOpenTag());
		sb.append(getTitle());
		sb.append(getCloseTag());
		return sb.toString();
	}
}
