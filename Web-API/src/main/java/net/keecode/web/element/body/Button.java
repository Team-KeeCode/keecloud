package net.keecode.web.element.body;

import net.keecode.web.element.BodyElement;

import java.lang.reflect.Method;

/**
 * Created by Michael on 04.04.2016.
 */
public class Button extends BodyElement {

	private Method executeOnClick;

	public Button() {
		super("button");
	}

}
