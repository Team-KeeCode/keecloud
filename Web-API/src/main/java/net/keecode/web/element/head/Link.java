package net.keecode.web.element.head;

import net.keecode.web.Attribute;
import net.keecode.web.element.HeadElement;

/**
 * Created by Michael on 04.04.2016.
 */
public class Link extends HeadElement {

	private Attribute rel;
	private Attribute type;
	private Attribute href;

	public Link() {
		super("head");
		rel = new Attribute("rel","");
		type = new Attribute("type","");
		href = new Attribute("href", "");

		addAttribute(rel);
		addAttribute(type);
		addAttribute(href);
	}

	public Attribute getRel() {
		return rel;
	}

	public void setRel(Attribute rel) {
		this.rel = rel;
	}

	public Attribute getType() {
		return type;
	}

	public void setType(Attribute type) {
		this.type = type;
	}

	public Attribute getHref() {
		return href;
	}

	public void setHref(Attribute href) {
		this.href = href;
	}

	@Override
	public String toString() {
		return getOpenTag();
	}
}
