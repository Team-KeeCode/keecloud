package net.keecode.web.element.head;

import net.keecode.web.Attribute;
import net.keecode.web.element.HeadElement;

/**
 * Created by Michael on 04.04.2016.
 */
public class Script extends HeadElement {

	private Attribute async;
	private Attribute charset;
	private Attribute defer;
	private Attribute src;
	private Attribute type;

	public Script(){
		super("script");
		async = new Attribute("async", "");
		charset = new Attribute("charset", "");
		defer = new Attribute("defer", "");
		src = new Attribute("src", "");
		type = new Attribute("type", "");

		addAttribute(async);
		addAttribute(charset);
		addAttribute(defer);
		addAttribute(src);
		addAttribute(type);
	}

	public Attribute getAsync() {
		return async;
	}

	public void setAsync(Attribute async) {
		this.async = async;
	}

	public Attribute getCharset() {
		return charset;
	}

	public void setCharset(Attribute charset) {
		this.charset = charset;
	}

	public Attribute getDefer() {
		return defer;
	}

	public void setDefer(Attribute defer) {
		this.defer = defer;
	}

	public Attribute getSrc() {
		return src;
	}

	public void setSrc(Attribute src) {
		this.src = src;
	}

	public Attribute getType() {
		return type;
	}

	public void setType(Attribute type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return getOpenTag();
	}
}
