package net.keecode.web.element;

import net.keecode.web.Attribute;

import java.util.ArrayList;

/**
 * Created by Michael on 04.04.2016.
 */
public class HtmlElement {

	private String tag;
	private ArrayList<HtmlElement> content = new ArrayList<>();
	private ArrayList<Attribute> attributes = new ArrayList<>();

	protected HtmlElement(String tag){this.tag = tag;}

	public String getOpenTag(){
		StringBuilder sb = new StringBuilder();
		sb.append("<");
		sb.append(getTag());
		for(Attribute attribute : attributes)
			if(!attribute.getValue().equals(""))
				sb.append(attribute);
		sb.append('>');
		return sb.toString();
	}

	public String getCloseTag(){return "</" + getTag() + '>';}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}

	public void addAttribute(Attribute attribute){
		attributes.add(attribute);
	}

	public void removeAttribute(Attribute attribute){
		content.remove(attribute);
	}

	public void setAttributes(ArrayList<Attribute> attributes) {
		this.attributes = attributes;
	}

	public ArrayList<HtmlElement> getContent() {
		return content;
	}

	public void addElement(HtmlElement element){
		content.add(element);
	}

	public void removeElement(HtmlElement element){
		content.remove(element);
	}

	public void removeElement(int position){
		content.remove(position);
	}

	public void setContent(ArrayList<HtmlElement> content) {
		this.content = content;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getOpenTag());
		for(HtmlElement element : getContent())
			sb.append(element);
		sb.append(getCloseTag());
		return sb.toString();
	}
}
