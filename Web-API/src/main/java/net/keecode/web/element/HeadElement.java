package net.keecode.web.element;

/**
 * Created by Michael on 04.04.2016.
 */
public abstract class HeadElement extends HtmlElement {

	protected HeadElement(String tag){
		super(tag);
	}

}
