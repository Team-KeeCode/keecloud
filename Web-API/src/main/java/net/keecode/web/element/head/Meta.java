package net.keecode.web.element.head;

import net.keecode.web.Attribute;
import net.keecode.web.element.HeadElement;
import org.w3c.dom.Attr;

import javax.smartcardio.ATR;

/**
 * Created by Michael on 04.04.2016.
 */
public class Meta extends HeadElement {

	private Attribute charset;
	private Attribute content;
	private Attribute httpequiv;
	private Attribute name;

	public Meta(){
		super("meta");
		charset = new Attribute("charset", "");
		content = new Attribute("content", "");
		httpequiv = new Attribute("http-equiv", "");
		name = new Attribute("name", "");

		addAttribute(charset);
		addAttribute(content);
		addAttribute(httpequiv);
		addAttribute(name);
	}

	public Attribute getCharset() {
		return charset;
	}

	public void setCharset(Attribute charset) {
		this.charset = charset;
	}

	public Attribute getMetaContent() {
		return content;
	}

	public void setMetaContent(Attribute content) {
		this.content = content;
	}

	public Attribute getHttpequiv() {
		return httpequiv;
	}

	public void setHttpequiv(Attribute httpequiv) {
		this.httpequiv = httpequiv;
	}

	public Attribute getName() {
		return name;
	}

	public void setName(Attribute name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return getOpenTag();
	}
}
