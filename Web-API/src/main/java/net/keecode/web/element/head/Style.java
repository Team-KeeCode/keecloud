package net.keecode.web.element.head;

import net.keecode.web.Attribute;
import net.keecode.web.element.HeadElement;

/**
 * Created by Michael on 04.04.2016.
 */
public class Style extends HeadElement {

	private Attribute media;
	private Attribute scoped;
	private Attribute type;

	public Style(){
		super("style");
		media = new Attribute("media", "");
		scoped = new Attribute("scoped","");
		type = new Attribute("type", "");

		addAttribute(media);
		addAttribute(scoped);
		addAttribute(type);
	}

	public Attribute getMedia() {
		return media;
	}

	public void setMedia(Attribute media) {
		this.media = media;
	}

	public Attribute getScoped() {
		return scoped;
	}

	public void setScoped(Attribute scoped) {
		this.scoped = scoped;
	}

	public Attribute getType() {
		return type;
	}

	public void setType(Attribute type) {
		this.type = type;
	}



}
