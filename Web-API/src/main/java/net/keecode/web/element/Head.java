package net.keecode.web.element;

import java.util.ArrayList;

/**
 * Created by Michael on 04.04.2016.
 */
public class Head {

	private ArrayList<HeadElement> content = new ArrayList<>();

	public void addElement(HeadElement element){
		content.add(element);
	}

	public void removeElement(HeadElement element){
		content.remove(element);
	}

	public void removeElement(int position){
		content.remove(position);
	}

	public void setContent(ArrayList<HeadElement> content) {
		this.content = content;
	}

	public ArrayList<HeadElement> getContent() {
		return content;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<head>");
		for(HeadElement element : content)
			sb.append(element);
		sb.append("</head>");
		return sb.toString();
	}
}
