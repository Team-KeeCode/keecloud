package net.keecode.web.servlet;

import net.keecode.util.HandlerMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Created by Michael on 04.04.2016.
 */
public class ButtonServlet extends HttpServlet {

	private HashMap<String, HandlerMethod> endpointMapping = new HashMap<>();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(endpointMapping.containsKey(req.getContextPath())){
			try{
				HandlerMethod m = endpointMapping.get(req.getContextPath());
				m.getMethod().invoke(m.getListener(), req, resp);
			}catch (IllegalAccessException | InvocationTargetException ex){
				ex.printStackTrace();
			}
		}
	}

}
