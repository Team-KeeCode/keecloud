package net.keecode.web;

import net.keecode.web.element.Body;
import net.keecode.web.element.Head;

/**
 * Created by Michael on 04.04.2016.
 */
public class Page {

	private Head head;
	private Body body;

	public Page() {
		head = new Head();
		body = new Body();
	}

	public Head getHead() {
		return head;
	}

	public void setHead(Head head) {
		this.head = head;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<html>");
		sb.append(head);
		sb.append(body);
		sb.append("</html>");
		return sb.toString();
	}
}
