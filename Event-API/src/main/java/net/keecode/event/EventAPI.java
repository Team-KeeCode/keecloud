package net.keecode.event;

import net.keecode.event.annotation.EventHandler;
import net.keecode.event.event.Event;
import net.keecode.exception.IllegalHandlerMethodException;
import net.keecode.listener.Listener;
import net.keecode.util.HandlerMethod;
import net.keecode.util.MashMap;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by Michael on 02.04.2016.
 */
public class EventAPI {

	private static EventAPI eventAPI;

	private MashMap<Class<? extends Event>, ArrayList<HandlerMethod>> handlerMap = new MashMap<Class<? extends Event>, ArrayList<HandlerMethod>>();


	private EventAPI() {
		eventAPI = this;
	}

	/**
	 * Get the singleton instance
	 *
	 * @return The instance of EventHandler
	 */
	public static EventAPI getInstance() {
		if (eventAPI == null)
			eventAPI = new EventAPI();
		return eventAPI;
	}

	/**
	 * Register handlers to be called when their requested event arrives
	 *
	 * @param listener The listener with the handler methods
	 */
	public synchronized void registerHandler(Listener listener) {
		for (Method method : listener.getClass().getDeclaredMethods()) {
			try {
				if (method.isAnnotationPresent(EventHandler.class)) {
					if (method.getParameterTypes()[0].getClass().getSuperclass() != Event.class)
						throw new IllegalHandlerMethodException(listener, method);
					Class<? extends Event> eventClass = (Class<? extends Event>) method.getParameterTypes()[0];
					addMethodHandler(HandlerMethod.getHandlerMethod(listener, method), eventClass);
				}
			} catch (IllegalHandlerMethodException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Actually adds the method to the handler list
	 *
	 * @param method   The method being added
	 * @param eventClass The eventClass of the event the method is waiting for
	 */
	private void addMethodHandler(HandlerMethod method, Class<? extends Event> eventClass) {
		ArrayList<HandlerMethod> handlers = new ArrayList<HandlerMethod>();
		if (handlerMap.containsKey(eventClass))
			handlers = (ArrayList<HandlerMethod>) handlerMap.getValue(eventClass).clone();
		handlers.add(method);
		handlerMap.removeKey(eventClass);
		handlerMap.put(eventClass, handlers);
	}

	/**
	 * Fire an event and emit it to all registered handlers
	 *
	 * @param event The event to fire
	 */
	public void fireEvent(Event event) {
		if (!handlerMap.containsKey(event.getClass()))
			return;
		ArrayList<HandlerMethod> handlers = (ArrayList<HandlerMethod>) handlerMap.getValue(event.getClass()).clone();
		for (HandlerMethod method : handlers) {
			Thread t = new Thread() {
				@Override
				public void run() {
					try {
						method.getMethod().setAccessible(true);
						method.getMethod().invoke(method.getListener(), event);
					} catch (IllegalAccessException | InvocationTargetException ex) {
						ex.printStackTrace();
					}
				}
			};
		}
	}
}
