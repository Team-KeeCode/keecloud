package net.keecode.network;

import io.netty.channel.Channel;
import net.keecode.listener.Listener;
import net.keecode.network.annotation.PacketHandler;
import net.keecode.exception.IllegalHandlerMethodException;
import net.keecode.network.packet.Packet;
import net.keecode.util.HandlerMethod;
import net.keecode.util.MashMap;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * The API for the PacketSystem built with netty
 * Handling, sending, registering packets is done here
 *
 * @author Team KeeCode
 */
public class NetworkAPI {

	private static NetworkAPI networkAPI;

	private MashMap<Class<? extends Packet>, ArrayList<HandlerMethod>> handlerMap = new MashMap<Class<? extends Packet>, ArrayList<HandlerMethod>>();
	private Channel defaultChannel;


	private NetworkAPI() {
		networkAPI = this;
	}

	/**
	 * Get the singleton instance
	 *
	 * @return The instance of NetworkAPI
	 */
	public static NetworkAPI getInstance() {
		if (networkAPI == null)
			networkAPI = new NetworkAPI();
		return networkAPI;
	}

	/**
	 * Register handlers to be called when their requested packet arrives
	 *
	 * @param listener The listener with the handler methods
	 */
	public synchronized void registerHandler(Listener listener) {
		for (Method method : listener.getClass().getDeclaredMethods()) {
			try {
				if (method.isAnnotationPresent(PacketHandler.class)) {
					if (method.getParameterTypes()[0].getClass().getSuperclass() != Packet.class)
						throw new IllegalHandlerMethodException(listener, method);
					Class<? extends Packet> packetClass = (Class<? extends Packet>) method.getParameterTypes()[0];
					addMethodHandler(HandlerMethod.getHandlerMethod(listener, method), packetClass);
				}
			} catch (IllegalHandlerMethodException ex) {
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Actually adds the method to the handler list
	 *
	 * @param method   The method being added
	 * @param packetClass The packetClass of the packet the method is waiting for
	 */
	private void addMethodHandler(HandlerMethod method, Class<? extends Packet> packetClass) {
		ArrayList<HandlerMethod> handlers = new ArrayList<HandlerMethod>();
		if (handlerMap.containsKey(packetClass))
			handlers = (ArrayList<HandlerMethod>) handlerMap.getValue(packetClass).clone();
		handlers.add(method);
		handlerMap.removeKey(packetClass);
		handlerMap.put(packetClass, handlers);
	}

	/**
	 * Get the default channel for sending packets
	 *
	 * @return The default channel
	 */
	public Channel getDefaultChannel() {
		return defaultChannel;
	}

	/**
	 * Set the default channel for sending packets
	 *
	 * @param defaultChannel The new default channel
	 */
	public void setDefaultChannel(Channel defaultChannel) {
		this.defaultChannel = defaultChannel;
	}

	/**
	 * Send a packet to the default channel
	 *
	 * @param packet The packet you want to send
	 */
	public void sendPacket(Packet packet) {
		sendPacket(packet, defaultChannel);
	}

	/**
	 * Send a packet to a custom channel
	 *
	 * @param packet  The packet you want to send
	 * @param channel The channel the packet should be send over
	 */
	public boolean sendPacket(Packet packet, Channel channel) {
		return channel.writeAndFlush(packet).isSuccess();
	}

	/**
	 * Handle an incoming packet
	 *
	 * @param packet The received packet
	 */
	public void handlePacket(Packet packet) {
		if (!handlerMap.containsKey(packet.getClass()))
			return;
		ArrayList<HandlerMethod> handlers = (ArrayList<HandlerMethod>) handlerMap.getValue(packet.getClass()).clone();
		for (HandlerMethod method : handlers) {
			Thread t = new Thread() {
				@Override
				public void run() {
					try {
						method.getMethod().setAccessible(true);
						method.getMethod().invoke(method.getListener(), packet);
					} catch (IllegalAccessException | InvocationTargetException ex) {
						ex.printStackTrace();
					}
				}
			};
		}
	}

}
