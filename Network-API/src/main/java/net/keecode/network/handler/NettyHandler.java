package net.keecode.network.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.keecode.network.NetworkAPI;
import net.keecode.network.packet.Packet;
import sun.nio.ch.Net;

/**
 * Created by Michael on 02.04.2016.
 */
public class NettyHandler extends SimpleChannelInboundHandler<Packet> {

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		super.channelActive(ctx);
		NetworkAPI.getInstance().setDefaultChannel(ctx.channel());
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		super.channelInactive(ctx);
		NetworkAPI.getInstance().setDefaultChannel(null);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet packet) throws Exception {

	}
}
