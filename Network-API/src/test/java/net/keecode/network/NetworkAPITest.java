package net.keecode.network;

import net.keecode.network.packet.Packet;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

/**
 * Created by Michael on 02.04.2016.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NetworkAPITest {

	@Test
	public void aGetInstance() throws Exception {
		assertTrue(NetworkAPI.getInstance() != null);
	}
}