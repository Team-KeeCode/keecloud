package net.keecode.util;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 * Created by Michael on 02.04.2016.
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MashMapTest {

	private MashMap<String, Integer> mashMap = new MashMap<String, Integer>();

	@Test
	public void bput() throws Exception {
		mashMap.put("Test", 1234);
	}

	@Test
	public void cgetValue() throws Exception {
		mashMap.put("Test", 1234);
		int value = mashMap.getValue("Test");
		Assert.assertEquals(1234, value);
	}

	@Test
	public void dgetKey() throws Exception {
		mashMap.put("Test", 1234);
		String key = mashMap.getKey(1234);
		Assert.assertEquals("Test", key);
	}

	@Test
	public void ekeys() throws Exception {
		mashMap.put("Test", 1234);
		Assert.assertTrue(mashMap.keys().contains("Test"));
	}

	@Test
	public void fvalues() throws Exception {
		mashMap.put("Test", 1234);
		Assert.assertTrue(mashMap.values().contains(1234));
	}

	@Test
	public void ggetKeyValueMap() throws Exception {
		mashMap.put("Test", 1234);
		Assert.assertTrue(mashMap.getKeyValueMap().containsKey("Test"));
		Assert.assertTrue(mashMap.getKeyValueMap().containsValue(1234));
	}

	@Test
	public void hgetValueKeyMap() throws Exception {
		mashMap.put("Test", 1234);
		Assert.assertTrue(mashMap.getValueKeyMap().containsKey(1234));
		Assert.assertTrue(mashMap.getValueKeyMap().containsValue("Test"));
	}

	@Test
	public void iremoveKey() throws Exception {
		mashMap.put("Test", 1234);
		mashMap.removeKey("Test");
		Assert.assertTrue(mashMap.getValue("Test") == null);
		Assert.assertTrue(mashMap.getKey(1234) == null);
	}

	@Test
	public void jremoveValue() throws Exception {
		mashMap.put("Test", 1234);
		mashMap.removeValue(1234);
		Assert.assertTrue(mashMap.getValue("Test") == null);
		Assert.assertTrue(mashMap.getKey(1234) == null);
	}

	@Test
	public void kremove() throws Exception {
		mashMap.put("Test", 1234);
		mashMap.remove("Test", 1234);
		Assert.assertTrue(mashMap.getValue("Test") == null);
		Assert.assertTrue(mashMap.getKey(1234) == null);
	}
}