package net.keecode.util;

import net.keecode.listener.Listener;

import java.lang.reflect.Method;

/**
 * Store the {@link Listener} instance for the {@Link Method}
 *
 * @author Team KeeCode
 */
public class HandlerMethod {

	private static MashMap<Listener, HandlerMethod> handlerMap = new MashMap<Listener, HandlerMethod>();

	private Method method;
	private Listener listener;

	/**
	 * Used to store the method and listener
	 *
	 * @param method   The handler method
	 * @param listener The listener in which the method is declared
	 */
	private HandlerMethod(Method method, Listener listener) {
		this.method = method;
		this.listener = listener;
		handlerMap.put(listener, this);
	}

	/**
	 * Returns an instance of HandlerMethod or creates a new one if no existing instance
	 * is found in the handlerMap
	 *
	 * @param listener The listener instance in which the method is declared
	 * @param method   The handler method
	 * @return The HandlerMethod instance
	 */
	public static HandlerMethod getHandlerMethod(Listener listener, Method method) {
		if (!handlerMap.containsKey(listener))
			new HandlerMethod(method, listener);
		return handlerMap.getValue(listener);
	}

	/**
	 * Returns the listener instance the method was found in
	 *
	 * @return The listener instance in order to invoke the method
	 */
	public Listener getListener() {
		return listener;
	}

	/**
	 * Returns the method that was declared as handler
	 *
	 * @return the method
	 */
	public Method getMethod() {
		return method;
	}
}
