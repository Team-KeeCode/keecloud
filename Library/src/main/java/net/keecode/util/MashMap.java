package net.keecode.util;

import net.keecode.exception.DoubleKeyException;
import net.keecode.exception.IllegalMashMapException;

import java.util.HashMap;
import java.util.Set;

/**
 * A HashMap with unique keys and values where you
 * can get the value with the key and the key
 * with the value
 *
 * @author Team KeeCode
 * @version 1.0
 */
public class MashMap<K, V> {

	private HashMap<K, V> keyValueMap = new HashMap<K, V>();
	private HashMap<V, K> valueKeyMap = new HashMap<V, K>();

	public MashMap() {
	}

	/**
	 * Create a MashMap based on an existing HashMap
	 *
	 * @param map The existing HashMap
	 */
	public MashMap(HashMap<K, V> map) {
		for (K key : map.keySet()) {
			put(key, map.get(key));
		}
	}

	/**
	 * Add an element to the MashMap
	 *
	 * @param key   The key object
	 * @param value The value object
	 */
	public void put(K key, V value) {
		try {
			if (keyValueMap.containsKey(key))
				throw new DoubleKeyException(DoubleKeyException.MashMapType.KEY_MAP);
			if (valueKeyMap.containsKey(value))
				throw new DoubleKeyException(DoubleKeyException.MashMapType.VALUE_MAP);

			keyValueMap.put(key, value);
			valueKeyMap.put(value, key);
		} catch (DoubleKeyException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Check if a key is stored in the MashMap
	 *
	 * @param key The key to be checked
	 * @return True if the key was found
	 */
	public boolean containsKey(K key) {
		return keyValueMap.containsKey(key) && valueKeyMap.containsValue(key);
	}

	/**
	 * Check if a value is stored in the MashMap
	 *
	 * @param value The value to be checked
	 * @return True if the value was found
	 */
	public boolean containsValue(V value) {
		return keyValueMap.containsValue(value) && valueKeyMap.containsKey(value);
	}

	/**
	 * Remove a key and its value from the MashMap
	 *
	 * @param key The key to be removed
	 */
	public void removeKey(K key) {
		try {
			if (!keyValueMap.containsKey(key))
				throw new IllegalMashMapException();
			remove(key, keyValueMap.get(key));

		} catch (IllegalMashMapException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Remove a key and value pair from the MashMap
	 *
	 * @param key   The key to be removed
	 * @param value The value to be removed
	 */
	public void remove(K key, V value) {
		try {

			if (!keyValueMap.containsKey(key))
				throw new IllegalMashMapException();

			if (!valueKeyMap.containsKey(value))
				throw new IllegalMashMapException();

			valueKeyMap.remove(value);
			keyValueMap.remove(key);

		} catch (IllegalMashMapException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Remove a value and its key from the MashMap
	 *
	 * @param value The value to be removed
	 */
	public void removeValue(V value) {
		try {
			if (!valueKeyMap.containsKey(value))
				throw new IllegalMashMapException();
			remove(valueKeyMap.get(value), value);

		} catch (IllegalMashMapException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Get the value stored behind a key
	 *
	 * @param key The key you want to get the value from
	 * @return The value stored behind the key
	 */
	public V getValue(K key) {
		if (!keyValueMap.containsKey(key))
			return null;
		return keyValueMap.get(key);
	}

	/**
	 * Get the key stored in front of the value
	 *
	 * @param value The value you want to get the key from
	 * @return The key stored in front of the value
	 */
	public K getKey(V value) {
		if (!valueKeyMap.containsKey(value))
			return null;
		return valueKeyMap.get(value);
	}

	/**
	 * Get all keys stored in the MashMap
	 *
	 * @return All keys
	 */
	public Set<K> keys() {
		return keyValueMap.keySet();
	}

	/**
	 * Get all values stored in the MashMap
	 *
	 * @return All values
	 */
	public Set<V> values() {
		return valueKeyMap.keySet();
	}

	/**
	 * Get a Key->Value HashMap from the MashMap
	 *
	 * @return A Key->Value HashMap
	 */
	public HashMap<K, V> getKeyValueMap() {
		return (HashMap<K, V>) keyValueMap.clone();
	}

	/**
	 * Get a Value->Key HashMap from the MashMap
	 *
	 * @return A Value->Key HashMap
	 */
	public HashMap<V, K> getValueKeyMap() {
		return (HashMap<V, K>) valueKeyMap.clone();
	}
}
