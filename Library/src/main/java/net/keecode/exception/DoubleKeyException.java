package net.keecode.exception;

/**
 * Created by Michael on 02.04.2016.
 */
public class DoubleKeyException extends Exception {

    private MashMapType type;

    public DoubleKeyException(MashMapType type){
        super("Tried to add key twice!");
        this.type = type;
    }

    public MashMapType getType() {
        return type;
    }

    public enum MashMapType {
        VALUE_MAP, KEY_MAP
    }
}