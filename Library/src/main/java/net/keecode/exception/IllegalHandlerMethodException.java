package net.keecode.exception;

import net.keecode.listener.Listener;

import java.lang.reflect.Method;

/**
 * Created by Michael on 02.04.2016.
 */
public class IllegalHandlerMethodException extends Exception {

	private Method method;
	private Listener listener;

	public IllegalHandlerMethodException(Listener listener, Method method){
		super("Illegal Method parameters! Found " + method.getParameterTypes()[0].getSimpleName());
		this.method = method;
		this.listener = listener;
	}

	public Method getMethod() {
		return method;
	}

	public Listener getListener() {
		return listener;
	}
}
