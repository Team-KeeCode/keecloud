package net.keecode.exception;

/**
 * Created by Michael on 02.04.2016.
 */
public class IllegalMashMapException extends Exception {

    public IllegalMashMapException(){
        super("Cant find Key & Value Pair");
    }
}